﻿
namespace Base2art.Timer
{
    using System;
    using System.Threading;
    using System.Windows.Forms;

    public class Program
    {
        /// <summary>Program entry point.</summary>
        /// <param name="args">Command Line Arguments</param>
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            bool isFirstInstance;
            // Please use a unique name for the mutex to prevent conflicts with other programs
            using (Mutex mtx = new Mutex(true, "Base2art.Timer", out isFirstInstance))
            {
                if (isFirstInstance)
                {
//					using (NotificationIcon notificationIcon = new NotificationIcon())
                    {
//						notificationIcon.notifyIcon.Visible = true;
                        Application.Run(new NotificationIcon());
                    }
                }
                else
                {
                }
            }
        }
    }
}
