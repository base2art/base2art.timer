﻿
namespace Base2art.Timer
{
    partial class TimerWindow
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timeSpanChooser = new System.Windows.Forms.DateTimePicker();
            this.startButton = new System.Windows.Forms.Button();
            this.remainingDisplay = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timeSpanChooser
            // 
            this.timeSpanChooser.CustomFormat = "HH:mm:ss";
            this.timeSpanChooser.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeSpanChooser.Location = new System.Drawing.Point(12, 12);
            this.timeSpanChooser.Name = "timeSpanChooser";
            this.timeSpanChooser.ShowUpDown = true;
            this.timeSpanChooser.Size = new System.Drawing.Size(82, 20);
            this.timeSpanChooser.TabIndex = 0;
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(601, 12);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(113, 23);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Start Countdown";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButtonClick);
            // 
            // remainingDisplay
            // 
            this.remainingDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.remainingDisplay.Font = new System.Drawing.Font("Segoe Print", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remainingDisplay.Location = new System.Drawing.Point(13, 39);
            this.remainingDisplay.Name = "remainingDisplay";
            this.remainingDisplay.Size = new System.Drawing.Size(701, 229);
            this.remainingDisplay.TabIndex = 2;
            this.remainingDisplay.Text = "1:00:00";
            this.remainingDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
            // 
            // TimerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 277);
            this.Controls.Add(this.remainingDisplay);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.timeSpanChooser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "TimerWindow";
            this.Text = "Timer";
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label remainingDisplay;
        private System.Windows.Forms.DateTimePicker timeSpanChooser;
    }
}
