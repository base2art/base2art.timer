﻿
using System;

namespace Base2art.Timer
{
    public interface IDataHolder
    {
        
        DateTime? EndOfDays { get; set; }
        
        DateTime OriginatingDate
        {
            get;
        }
    }
}
