﻿
namespace Base2art.Timer
{
	using System;
	using System.Windows.Forms;
    public class MenuItemFactory
    {
        public MenuItem[] InitializeMenu()
        {
            MenuItem[] menu = new MenuItem[] {
                new MenuItem("About", menuAboutClick),
                new MenuItem("Exit", menuExitClick)
            };
            return menu;
        }
        
        private void menuAboutClick(object sender, EventArgs e)
        {
            MessageBox.Show("About This Application");
        }
        
        private void menuExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
