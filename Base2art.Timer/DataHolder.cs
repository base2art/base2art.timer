﻿
namespace Base2art.Timer
{
	using System;
	
    public class DataHolder : IDataHolder
    {

        public DateTime? EndOfDays { get; set; }

        public DateTime OriginatingDate
        {
            get
            {
                return new DateTime(2000, 01,01, 0,0,0, DateTimeKind.Utc);
            }
        }
    }
}
