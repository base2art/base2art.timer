﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Base2art.Timer
{
    public sealed class NotificationIcon : ApplicationContext
    {
        private NotifyIcon notifyIcon;
        private ContextMenu notificationMenu;

        private TimerWindow form;
        private readonly DataHolder dataHolder = new DataHolder();
        
        public NotificationIcon()
        {
            Application.ApplicationExit += new EventHandler(this.OnApplicationExit);
            this.InitializeComponent();
            this.notifyIcon.Visible = true;
            
        }

		private void InitializeComponent()
		{
		    this.notifyIcon = new NotifyIcon();
		    this.notificationMenu = new ContextMenu(new MenuItemFactory().InitializeMenu());
            
            // Open Form
            //            this.notifyIcon.DoubleClick += IconDoubleClick;
            this.notifyIcon.Click += IconDoubleClick;
            // Setup Icon
            this.notifyIcon.Icon = ResourceAccess.TimerIcon;
            this.notifyIcon.Text = "Timer";

            this.notifyIcon.ContextMenu = notificationMenu;
            Setup();
            this.notifyIcon.Visible = false;
            this.notifyIcon.Visible = true;
            
		}
		
        private void OnApplicationExit(object sender, EventArgs e)
        {
            //Cleanup so that the icon will be removed when the application is closed
            try
            {
                this.notifyIcon.Visible = false;
            }
            catch
            {
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.notifyIcon.Visible = false;
                this.notificationMenu.Dispose();
                this.notifyIcon.Dispose();
            }
        }

        void Setup()
        {
            if (this.form != null && this.form.IsDisposed)
            {
                this.form = null;
            }
            
            if (this.form == null)
            {
                this.form = new TimerWindow();
            }
            
            this.form.ParentData = this.dataHolder;
            this.form.Show();
        }
        
        private void IconDoubleClick(object sender, EventArgs e)
        {
            this.Setup();
        }
    }
}
