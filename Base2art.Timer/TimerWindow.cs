﻿
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace Base2art.Timer
{
    /// <summary>
    /// Description of TimerWindow.
    /// </summary>
    public partial class TimerWindow : Form
    {
        private IDataHolder parentData;
        
        public TimerWindow()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            this.Icon = ResourceAccess.TimerIcon;
            InitializeComponent();
        }
        
        public IDataHolder ParentData
        {
            get
            {
                return this.parentData;
            }
            set
            {
                this.parentData = value;
                var minDate = this.parentData.OriginatingDate;
                this.timeSpanChooser.Value = minDate.AddHours(1);
                this.timeSpanChooser.Invalidate();
                
                var eod = this.ParentData.EndOfDays;
                if (eod.HasValue)
                {
                    this.SetupTimer();
                }
                else
                {
                    this.timer1.Enabled = false;
                }
            }
        }
        
        private void StartButtonClick(object sender, EventArgs e)
        {
            var offset = this.timeSpanChooser.Value.Subtract(this.timeSpanChooser.Value.Date);
            this.ParentData.EndOfDays = DateTime.UtcNow.Add(offset);
            this.SetupTimer();
        }
        
        private void Timer1Tick(object sender, EventArgs e)
        {
            var eod = this.ParentData.EndOfDays;
            if (eod.HasValue)
            {
                var remaining = eod.Value.Subtract(DateTime.UtcNow);
                var format= this.timeSpanChooser.CustomFormat.ToLowerInvariant().Replace(":", "\\:");
                this.remainingDisplay.Text = remaining.ToString(format);
            }
        }

        private void SetupTimer()
        {
            this.timer1.Enabled = false;
            this.timer1.Interval = (int)TimeSpan.FromSeconds(1).TotalMilliseconds;
            this.Timer1Tick(this, EventArgs.Empty);
            this.timer1.Start();
        }
    }
}
