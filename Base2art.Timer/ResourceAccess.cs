﻿
namespace Base2art.Timer
{
    using System;
	using System.ComponentModel;
    using System.Drawing;

    public static class ResourceAccess
    {
        public static Icon TimerIcon
        {
            get
            {
                ComponentResourceManager resources = new ComponentResourceManager(typeof(NotificationIcon));
                return (Icon)resources.GetObject("$this.Icon");
            }
        }
    }
}
